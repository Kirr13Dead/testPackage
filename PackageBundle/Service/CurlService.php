<?php

namespace kirr\PackageBundle\Service;

/**
 * Class CurlServices
 *
 * @package ConsoleBundle\Service
 */
class CurlService
{
    /* methods */
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    const METHOD_GET = 'GET';

    private $curl;
    private $method;
    private $data = [];
    private $url;
    private $user;
    private $password;
    private $result;
    private $status;

    /**
     * Authentication
     */
    private function authentication()
    {
        $logIn = (isset($this->user) && isset($this->password))
            ? sprintf('%s:%s', $this->user, $this->password) : '';

        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->curl, CURLOPT_USERPWD, $logIn);
        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    }

    /**
     * Send POST
     */
    private function sendPost()
    {
        curl_setopt($this->curl, CURLOPT_POST, 1);

        if (count($this->data) > 0) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->data);
        }
    }

    /**
     * Send PUT
     */
    private function sendPut()
    {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, self::METHOD_PUT);
        if (count($this->data) > 0) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->data);
        }
    }

    /**
     * Send DELETE
     */
    private function sendDelete()
    {
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, self::METHOD_DELETE);
        if (count($this->data) > 0) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->data);
        }
    }

    /**
     * Send GET
     */
    private function sendGet()
    {
        if ($this->data) {
            $this->url = sprintf("%s?%s", $this->url, $this->data);
        }
    }

    /**
     * @param       $method
     * @param       $url
     * @param array $data
     * @param bool  $buildQuery
     * @param null  $user
     * @param null  $password
     */
    public function cell($method, $url, $data = [], $buildQuery = true, $user = null, $password = null)
    {
        $this->unSetValue();

        $this->curl = curl_init();

        if (!isset($method) || !isset($url)) {
            new \Exception('Не задан $method и $url!');
        }

        $this->method = $method;
        $this->url = $url;
        $this->data = $buildQuery ? http_build_query($data) : $data;

        switch ($method) {
            case self::METHOD_POST:
                $this->sendPost();
                break;

            case self::METHOD_PUT:
                $this->sendPut();
                break;

            case self::METHOD_DELETE:
                $this->sendDelete();
                break;

            default:
                $this->sendGet();
                break;
        }

        if (!is_null($password)) {
            $this->password = $password;
        }

        if (!is_null($user)) {
            $this->user = $user;
        }

        $this->authentication();

        $this->result = curl_exec($this->curl);
        $this->status = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        curl_close($this->curl);
    }

    /**
     * @param bool $decode
     *
     * @return mixed
     */
    public function getResult($decode = false)
    {

        return $decode ? json_decode($this->result, true) : $this->result;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return (int) $this->status;
    }

    /**
     * Unset work variables
     */
    private function unSetValue()
    {
        unset($this->method);
        unset($this->data);
        unset($this->url);
        unset($this->user);
        unset($this->password);
        unset($this->result);
        unset($this->status);
    }
}
